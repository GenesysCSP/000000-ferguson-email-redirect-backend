const AWSXRay = require('aws-xray-sdk-core')
const AWS = AWSXRay.captureAWS(require('aws-sdk'))
AWS.config.update({ region: process.env.REGION })
const ses = new AWS.SES()

const sendEmail = async (sender, recipient, subject, bodyHtml, charset = 'UTF-8') => {
    const params = {
        Source: sender,
        Destination: {
            ToAddresses: [
                recipient
            ],
        },
        Message: {
            Subject: {
                Data: subject,
                Charset: charset
            },
            Body: {
                Html: {
                    Data: bodyHtml,
                    Charset: charset
                }
            }
        }
    }
    return ses.sendEmail(params).promise()
}

module.exports = {
    sendEmail
}