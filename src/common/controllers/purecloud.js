const requestPromise = require('request-promise')
const fetch = require('node-fetch')

const fetchWrapper = async (url, init) => {
    const response = await fetch(url, init)
    let correlationId, retryAfterMs
    for (let pair of response.headers.entries()) {
        if (pair[0] === 'inin-correlation-id')
            correlationId = pair[1]

        if (pair[0] === 'retry-after')
            retryAfterMs = pair[1] * 1000
    }
    const json = await response.json()
    return response.ok
        ? { ...json, correlationId }
        : Promise.reject({ ...json, correlationId, retryAfterMs })
}

const getToken = (env, id, secret) => {
    return requestPromise.post({
        url: `https://login.${env}/oauth/token`,
        form: { grant_type: 'client_credentials' },
        auth: {
            user: id,
            password: secret,
            sendImmediately: true
        }
    })
}

// example use of fetchWrapper
const getUsers = (env, token, uri) => {
    return fetchWrapper(`https://api.${env}${uri}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `bearer ${token}`
        }
    })
}

module.exports = {
    getToken,
    getUsers
}