const AWSXRay = require('aws-xray-sdk-core')
const AWS = AWSXRay.captureAWS(require('aws-sdk'))
const dataPipeLine = new AWS.DataPipeline()

/**
 *  https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DataPipeline.html
 * 
 *  */
const deletePipeline = (pipeLineName) => {
    return new Promise((resolve, reject) => {
        dataPipeLine.listPipelines({ marker: '' }, (err, data) => {
            if (err) {
                console.log(err, err.stack)
                return reject(err)
            }
            else {
                console.log(data)
                for (var i in data.pipelineIdList) {
                    if (data.pipelineIdList[i].name == pipeLineName) {
                        let pipeLineToDelete = {
                            pipelineId: data.pipelineIdList[i].id
                        };
                        dataPipeLine.deletePipeline(pipeLineToDelete, (err, data) => {
                            if (err) {
                                console.log(err, err.stack)
                                return reject(err)
                            }
                            else {
                                return resolve(`Old clone deleted ${pipeLineToDelete.pipelineId}. Create new clone now`)
                            }
                        });
                        break
                    }
                }
                return resolve('No clones found to delete. Create a new clone now')
            }
        });
    })
}
const createAndUpdatePipeLine = (pipeLineName, templatePipeline, inputS3Loc, tableName) => {
    return new Promise((resolve, reject) => {
        dataPipeLine.getPipelineDefinition(templatePipeline, (err, definition) => {
            if (err) {
                console.log(err, err.stack)
                return reject(err)
            }
            else {
                const pipeLine = {
                    name: pipeLineName,
                    uniqueId: pipeLineName
                }
                dataPipeLine.createPipeline(pipeLine, (err, pipelineIdObject) => {
                    if (err) {
                        console.log(err, err.stack)
                        return reject(err)
                    }
                    else { //new pipeline created with id=pipelineIdObject.pipelineId
                        console.log(pipelineIdObject)
                        definition.parameterValues.forEach((parameterValue) => {
                            switch (parameterValue.id) {
                                case 'myDDBRegion': parameterValue.stringValue = process.env.REGION;
                                    break;
                                case 'myDDBWriteThroughputRatio': parameterValue.stringValue = '1';
                                    break;
                                case 'myDDBTableName': parameterValue.stringValue = tableName;
                                    break;
                                case 'myInputS3Loc': console.log('myInputS3Loc:', inputS3Loc);
                                    parameterValue.stringValue = inputS3Loc;
                                    break;
                                default:
                                    break;
                            }
                        })

                        const pipelineDefinitions = {
                            pipelineId: pipelineIdObject.pipelineId,
                            pipelineObjects: definition.pipelineObjects,
                            parameterObjects: definition.parameterObjects,
                            parameterValues: definition.parameterValues
                        }
                        dataPipeLine.putPipelineDefinition(pipelineDefinitions, (err, data) => {
                            if (err) {
                                console.log(err, err.stack)
                                return reject(err)
                            }
                            else {
                                console.log('updated the pipeline definition')
                                return resolve(pipelineIdObject)
                            }
                        });
                    }
                })
            }
        })

    })
}
const activatePipeLine = (pipelineIdObject) => {
    return new Promise((resolve, reject) => {
        dataPipeLine.activatePipeline(pipelineIdObject, (err, data) => {
            if (err) {
                console.log(err, err.stack)
                return reject(err)
            }
            else {
                console.log(data)
                return resolve('activated the pipeline')
            }
        });
    })
}

module.exports = {
    deletePipeline,
    createAndUpdatePipeLine,
    activatePipeLine
};  