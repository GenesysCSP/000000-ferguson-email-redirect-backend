const AWSXRay = require('aws-xray-sdk-core')
const AWS = AWSXRay.captureAWS(require('aws-sdk'))
const S3 = new AWS.S3()

const upload = (bucket, key, body, contentType) => {
    const params = {
        Bucket: bucket,
        Key: key,
        Body: body,
        ContentType: contentType
    };
    return new Promise((resolve, reject) => {
        console.log(`Uploading file ${params.Key} to S3 bucket: ${params.Bucket}`);
        S3.upload(params, (err, data) => {
            if (err) {
                console.log('Upload failure')
                return reject(err);
            }
            console.log('Upload success');
            return resolve(data);
        });
    })
}

const listObjects = (bucket, folder, delimiter, maxKeys = 1000) => {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: bucket,
            Prefix: folder,
            MaxKeys: maxKeys,
            Delimiter: delimiter
        }
        console.log(`Listing files prefixed by ${params.Prefix} from S3 bucket: ${params.Bucket}`);
        S3.listObjectsV2(params, (err, data) => {
            if (err) {
                console.log(`There was an error listing files: ${err}`);
                return reject(err);
            }
            return resolve(data);
        });
    })
}

const getObject = (bucket, key) => {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: bucket,
            Key: key
        }
        //  console.log(`Reading file ${params.Key} from bucket ${params.Bucket}`);
        S3.getObject(params, (err, data) => {
            if (err) {
                console.log(`There was an error retrieving the object ${params.Key}:${err}`)
                return reject(err)
            }
            //   console.log('Object retrieved successfully')
            return resolve({
                ...data,
                Key: key
            });
        });
    })
}

const deleteObject = (bucket, key) => {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: bucket,
            Key: key
        }
        console.log(`Deleting file ${params.Key} from bucket ${params.Bucket}`);
        S3.deleteObject(params, (err, data) => {
            if (err) {
                console.log('There was an error deleting the object')
                return reject(err);
            }
            console.log('Object deleted successfully')
            return resolve(data);
        });
    })
}

const headObject = (bucket, key) => {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: bucket,
            Key: key
        }
        console.log(`Retrieving metadata for object ${params.Key} from bucket ${params.Bucket}`);
        S3.headObject(params, (err, metadata) => {
            if (err) {
                console.log('There was an error retrieving the object metadata')
                return reject(err)
            }
            console.log('Object metadata retrieved successfully')
            return resolve(metadata);
        })
    })
}

const getSignedUrl = (operation, bucket, key, expires, contentType) => {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: bucket,
            Key: key
        }
        if (contentType) {
            params.ContentType = contentType
        }
        if (expires) {
            params.Expires = expires;
        }
        console.log(`Retrieving signed URL (Operation = ${operation}) for object ${params.Key} on bucket ${params.Bucket}`);
        S3.getSignedUrl(operation, params, (err, data) => {
            if (err) {
                console.log('There was an error retrieving the signed URL')
                return reject(err)
            }
            console.log('Signed URL retrieved successfully')
            return resolve(data);
        })
    })
}

module.exports = {
    upload,
    listObjects,
    getObject,
    deleteObject,
    headObject,
    getSignedUrl
}