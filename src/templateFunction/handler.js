const AWSXRay = require('aws-xray-sdk-core')
const lambda = require('../common/controllers/lambda')
const utils = require('../common/utils/utils')
const statusCodes = require('../common/constants/statusCodes')
const moment = require('moment')

exports.index = async (event, context) => {
    const responseBody = { success: false }
    let segment, subSegment, customerId, customerName
    let status = statusCodes.InternalServerError

    try {
        // X-Ray setup
        segment = AWSXRay.getSegment()
        subSegment = segment.addNewSubsegment("INSERT MEANINGFUL SEGMENT NAME")

        // Token validation
        let response = await lambda.callLambdaFunction({}, event.headers, process.env.VALIDATE_TOKEN_LAMBDA)
        console.log('Token validation response: ', response)
        const body = JSON.parse(response.body)
        if (response.statusCode !== statusCodes.OK) {
            status = statusCodes.Unauthorized
            throw new Error(body.message)
        }

        // Subscription Check
        customerId = body.organization
        response = await lambda.callLambdaFunction({ customerId, applicationId: process.env.APP_ID }, event.headers, process.env.SUBSCRIPTION_CHECK_LAMBDA)
        customerName = JSON.parse(response.body).customerName
        if (response.statusCode !== statusCodes.OK) {
            status = statusCodes.Unauthorized
            throw new Error(JSON.parse(response.body).message)
        }

        // Headers validation
        response = utils.isRequestHeadersValid(event, ['token', 'tokensource'])
        if (!response.valid) {
            status = statusCodes.BadRequest
            throw new Error(response.message)
        }

        // Body validation
        response = utils.isRequestBodyValid(event, ['mandatory_body_property_1', 'mandatory_body_property_2'])
        if (!response.valid) {
            status = statusCodes.BadRequest
            throw new Error(response.message)
        }
        const requestBody = response.body

        // Business logic

        // 200 OK sample response
        responseBody.success = true
        responseBody.message = 'INSERT MEANINGFUL MESSAGE'
        status = statusCodes.OK
        return utils.sendResponse(status, responseBody)
    } catch (error) {
        responseBody.message = error.message
        if (status >= 500) {
            const payload = {
                summary: error.message,
                severity: 'critical',
                source: context.functionName,
                routing_key: process.env.PAGER_DUTY_SERVICE_INTEGRATION_KEY,
                customerId,
                customerName
            }
            await lambda.callLambdaFunction(payload, event.headers, process.env.TRIGGER_INCIDENT_LAMBDA)
        }
        return utils.sendResponse(status, responseBody)
    }
    finally {
        //X-Ray
        if (subSegment) {
            subSegment.addAnnotation("Success", responseBody.success)
            subSegment.addAnnotation("Message", responseBody.message)
            subSegment.addAnnotation("Status", status)
            subSegment.close()
        }
    }
}